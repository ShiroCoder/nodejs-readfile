
module.exports.kgConverter = function(num) {
    return Number.parseFloat(num*2.20462262).toFixed(2);
}

module.exports.ozConverter = function  (num) {
    return Number.parseFloat(num*0.0625).toFixed(2);
}

module.exports.gConverter = function (num) {
    return Number.parseFloat(num*0.00220462262).toFixed(2);
}

